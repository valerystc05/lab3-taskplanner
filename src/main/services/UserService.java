package main.services;

import main.model.pojo.User;


/**
 * Created by home-pc on 22.04.2017.
 */
public interface UserService {
    User auth(String login, String password);
}
