package main.services;

import main.model.dao.TaskDao;
import main.model.dao.TaskDaoImpl;
import main.model.pojo.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by home-pc on 23.04.2017.
 */
public class TaskServiceImpl implements TaskService{

    private static TaskDao taskDao = new TaskDaoImpl();

    @Override
    public List<Task> getAllTasks() {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(taskDao.getAll());
        return tasks;
    }
}
