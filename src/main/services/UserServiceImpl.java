package main.services;

import org.apache.log4j.Logger;
import main.model.dao.UserDao;
import main.model.dao.UserDaoImpl;
import main.model.pojo.User;

/**
 * Created by home-pc on 22.04.2017.
 */
public class UserServiceImpl implements UserService  {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    private static UserDao userDAO = new UserDaoImpl();

    @Override
    public User auth(String login, String password) {
        User user = userDAO.findUserByLoginAndPassword(login, password);
        logger.debug("user: " + user);

        //if (user != null) {
        //    return null;
        //}
        //logger.debug("user not blocked");

        return user;
    }

}
