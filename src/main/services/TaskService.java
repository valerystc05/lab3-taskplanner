package main.services;

import main.model.pojo.Task;

import java.util.List;

/**
 * Created by home-pc on 23.04.2017.
 */
public interface TaskService {

    List<Task> getAllTasks();
}
