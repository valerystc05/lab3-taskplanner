<!--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
-->
<html>
<head>
    <title>Список задач | Планировщик задач</title>
</head>
<body>

<h1>
Список задач:
</h1>
<table>
    <c:forEach items="${requestScope.tasks}" var="task">
        <tr>
            <td><c:out value="${task.taskId}"></c:out></td>
            <td><c:out value="${task.taskCreator}"></c:out></td>
            <td><c:out value="${task.taskPerformer}"></c:out></td>
            <td><c:out value="${task.taskDescription}"></c:out></td>
            <td><c:out value="${task.taskStatus}"></c:out></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
