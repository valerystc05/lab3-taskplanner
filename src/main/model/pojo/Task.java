package main.model.pojo;

/**
 * Created by home-pc on 23.04.2017.
 */
public class Task {

    private int taskId;
    private User taskCreator;
    private User taskPerformer;
    private String taskDescription;
    private int taskStatus;
/*
    public Task(int taskId, User taskCreator, User taskPerformer, String taskDescription, int taskStatus) {
        this.taskId = taskId;
        this.taskCreator = taskCreator;
        this.taskPerformer = taskPerformer;
        this.taskDescription = taskDescription;
        this.taskStatus = taskStatus;
    }
*/

    @Override
    public String toString() {
        return ("Task: "+taskId+" "+ taskCreator+" "+taskPerformer+" "+taskDescription+" "+taskStatus);
    }

    public int getTaskId() {
        return taskId;
    }

    public User getTaskCreator() {
        return taskCreator;
    }

    public User getTaskPerformer() {
        return taskPerformer;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public int getTaskStatus() {
        return taskStatus;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public void setTaskCreator(User taskCreator) {
        this.taskCreator = taskCreator;
    }

    public void setTaskPerformer(User taskPerformer) {
        this.taskPerformer = taskPerformer;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public void setTaskStatus(int taskStatus) {
        this.taskStatus = taskStatus;
    }
}
