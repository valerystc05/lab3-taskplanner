package main.model.dao;

import main.model.ConnectionPool;
import main.model.pojo.Task;
import main.model.pojo.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by home-pc on 23.04.2017.
 */
public class TaskDaoImpl implements TaskDao  {

    private static final String SELECT_ALL = "SELECT * FROM task";

    private static final String INSERT_INTO = "INSERT INTO task (task_creator_id, task_performer_id, task_description, task_status) " +
            "VALUES (?, ?, ?, ?)";

    private static final String UPDATE_WHERE = "UPDATE task SET task_creator_id=?, task_performer_id=?, " +
            "task_description=?, task_status=? WHERE id = ?";

    private static final String DELETE_BY_ID = "DELETE FROM task WHERE id=?";



    @Override
    public Collection<Task> getAll() {
        Set<Task> tasks = new HashSet<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery("SELECT * FROM task");
            while (resultSet.next()) {
                //tasks.add(createEntity(resultSet));
                Task task;
                task=createEntity(resultSet);
                tasks.add(task);
                task.toString();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tasks;
    }

    private Task createEntity(ResultSet resultSet) throws SQLException {
        Task task = new Task();

        task.setTaskId(resultSet.getInt("task_id"));
        task.setTaskDescription(resultSet.getString("task_description"));
        task.setTaskStatus(resultSet.getInt("task_status"));

        int creator_id = resultSet.getInt("task_creator_id");
        int performer_id = resultSet.getInt("task_performer_id");

        User taskCreator = new UserDaoImpl().getById(creator_id);
        User taskPerformer = new UserDaoImpl().getById(performer_id);

        task.setTaskCreator(taskCreator);
        task.setTaskPerformer(taskPerformer);

        return task;
    }
}
