package main.model.dao;

import java.util.Collection;

/**
 * Created by home-pc on 23.04.2017.
 */
public interface TaskDao <PK, E>{

    Collection<E> getAll();
}
