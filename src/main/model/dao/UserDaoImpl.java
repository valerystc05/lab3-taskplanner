package main.model.dao;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.model.ConnectionPool;
import main.model.pojo.User;
import org.apache.log4j.PropertyConfigurator;


/**
 * Created by home-pc on 22.04.2017.
 */
public class UserDaoImpl implements UserDao {

    static {
        PropertyConfigurator.configure("log4j.properties");//for properties
    }
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM user ";

    public User findUserByLoginAndPassword(String login, String password) {
        User user = null;
        //System.out.println("12345User to find: login - "+login+" password - "+password);
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection
                     .prepareStatement( "SELECT * FROM taskplanner.user WHERE user_login = ? AND user_password = ?")) {

            statement.setString(1, login);
            statement.setString(2, password);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = createEntity(resultSet);
            }

            //logger.debug("user " + user);
        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        }

        return user;
    }

    private User createEntity(ResultSet resultSet) throws SQLException {

        int var1 = resultSet.getInt("user_id");
        String var2 = resultSet.getString("user_login");
        String var3 = resultSet.getString("user_password");
        String var4 = resultSet.getString("user_firstname");
        String var5 = resultSet.getString("user_lastname");
        //System.out.println("var2" +var3);
        //System.out.println("var3" +var3);

        User user = new User(var1, var2, var3, var4, var5);
        return user;
    }

    public User getById(int id) {
        User user = null;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection
                     .prepareStatement(SELECT_ALL + " WHERE id = ?")) {

            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = createEntity(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

}
