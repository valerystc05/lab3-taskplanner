package main.model.dao;


import org.apache.log4j.Logger;
import main.model.dao.UserDao;
import main.model.dao.UserDaoImpl;
import main.model.pojo.User;

/**
 * Created by home-pc on 22.04.2017.
 */

public interface UserDao {

        User findUserByLoginAndPassword(String login, String password);

}
