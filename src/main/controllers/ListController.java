package main.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.model.pojo.Task;
import main.model.pojo.User;
import main.services.TaskService;
import main.services.TaskServiceImpl;
import main.services.UserService;
import main.services.UserServiceImpl;


/**
 * Created by home-pc on 23.04.2017.
 */
public class ListController extends HttpServlet  {

    private static TaskService taskService = new TaskServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Task> tasks = taskService.getAllTasks();

        System.out.println(tasks);
        System.out.println("privet");

        req.setAttribute("tasks", tasks);

        getServletContext().getRequestDispatcher("/listTasks.jsp")
                .forward(req, resp);
    }

}
