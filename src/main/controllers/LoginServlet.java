package main.controllers;

import main.services.UserService;
import main.services.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by home-pc on 20.04.2017.
 */
public class LoginServlet  extends HttpServlet {

    //private static final Logger logger = Logger.getLogger(LoginServlet.class);

    private static UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        //System.out.println("login "+login);
        //System.out.println("password "+password );


        if (userService.auth(login,password)!=null) {
            req.getSession().setAttribute("userLogin", login);

            //logger.debug("user: " + login + " logged" );
            //resp.sendRedirect(req.getContextPath() + "/listStudents");
            resp.sendRedirect("/listTasks");
        } else {
            resp.sendRedirect("/error");
        }

    }
}
